# HA cluster v2 - Rockpi 4A+
Second home HA cluster with Rockpi 4A+ (booting from 4MB SPI with 29GiB emmc, 4GB LPDDR4).  Debian 11 with Kernel 5.18.3. 


<pre>
Disk Partions: GPT
Number  Start (sector)    End (sector)  Size       Code  Name
   1           32768         4227071   2.0 GiB     8300  SYSTEM
   2         4227072        21004287   8.0 GiB     8300  DATA
   3        21004288        60620766   18.9 GiB    8300  Linux filesystem
</pre>

### Base system : 
Debian bootstrap + vim openssh-server gdisk dosfstools zram-tools wireguard-tools usbutils parted iperf iproute2 iptables haveged hdparm  squashfs-tools curl wget ca-certificates openssl python3 ntpsec pciutils 

## SYSTEM 
<pre>
-rw-r--r-- 1 root root 94777344 Nov 20 10:30 debian.squash
-rw-r--r-- 1 root root 35584512 Nov 15 19:32 Image
-rw-r--r-- 1 root root  3309632 Oct 19 12:57 initrd
-rw-r--r-- 1 root root  9207808 Nov 15 19:33 kmod.squash
drwx------ 2 root root     4096 Nov 15 13:05 lost+found
-rw-r--r-- 1 root root    78091 Nov 15 18:35 rk3399-rock-pi-4a.dtb
-rw-rw-r-- 1 1000 1000   364544 Nov 16 09:31 spiloader.img
-rw-rw-r-- 1 1000 1000   959448 Nov 16 09:31 u-boot.itb

</pre>
## Prerequsites:
Add to /etc/hosts. The cluster ip is 192.168.124.8
<pre>
192.168.124.10 hc1
192.168.124.9	hc0
</pre>

install packages :
<pre>apt-get install pacemaker corosync crmsh</pre>

generate corosync key and copy to other node
<pre>corosync-keygen</pre>

start corosync, pacemaker on both nodes. Stonith is disabled for first run
<pre>
crm configure property stonith-enabled=false
crm configure property no-quorum-policy=ignore
systemctl start corosync
systemctl start pacemaker
</pre>
enable cluster IP 
<pre>crm configure primitive ip ocf:heartbeat:IPaddr2 params ip=192.168.124.8 cidr_netmask="24" op monitor interval="30s"</pre>

All crm configure commands are stored in cib. This can be erased by
<pre>cibadmin -E --force</pre>

load cib as : 
<pre>crm configure load replace cib.txt</pre>

add node user, add sudo, and give node user sudo
install node on BOTH nodes as node user
adjust cib.txt to mount /home - this is what drbd syncs

<pre>bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)</pre>

Any node-red palette additons go to /home/node, hence will get synced across drbd

