resource r0 {
        protocol C;
        device /dev/drbd0;
        disk /dev/mmcblk1p3;
        meta-disk internal;
        on hc0 {
                address 192.168.124.9:7788;
        }
        on hc1 {
                address 192.168.124.10:7788;
        }
}

